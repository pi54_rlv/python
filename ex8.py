def paper_rock(user1,user2):
    print("first user:"+user1)
    print("second user:"+user2)
    if user1==user2:
        print("it is a tie")
    elif user1=='Rock':
        if user2 == 'Scissors':
            return("Rock wins!")
        else:
            return("Paper wins!")
    elif user1=="Scissors":
        if user2=="Rock":
            return("Rock wins!")
        else:
            return("Scissors wins!")
    elif user1=="Paper":
        if user2=="Scissors":
            return("Scissors wins!")
        else:
            return("Paper wins")

print(paper_rock('Rock','Paper'))
