import requests
from bs4 import BeautifulSoup
import json


url = 'https://www.allsides.com/media-bias/media-bias-ratings'

r = requests.get(url)

soup = BeautifulSoup(r.content, 'html.parser')
rows = soup.select('tbody tr')

row=rows[0]
name = row.select_one('.source-title').text.strip()
print(name)

allsides_page = row.select_one('.source-title a')['href']
allsides_page = 'https://www.allsides.com' + allsides_page

print(allsides_page)


bias = row.select_one('.views-field-field-bias-image a')['href']
bias = bias.split('/')[-1]

print(bias)

agree = row.select_one('.agree').text
agree = float(agree)

disagree = row.select_one('.disagree').text
disagree = float(disagree)

agree_ratio = agree / disagree
print("{:.2f}".format(agree_ratio))

# print(row.select_one('.community-feedback-rating-page'))

# for row in rows:
#     name = row.select_one('.source-title').text.strip()
#     print(name),
#     bias = row.select_one('.views-field-field-bias-image a')['href']
#     bias = bias.split('/')[-1]
#     print(bias)

data=[]
for row in rows:
    d = dict()

    d['name'] = row.select_one('.source-title').text.strip()
    d['allsides_page'] = 'https://www.allsides.com' + row.select_one('.source-title a')['href']
    d['bias'] = row.select_one('.views-field-field-bias-image a')['href'].split('/')[-1]
    d['agree'] = int(row.select_one('.agree').text)
    d['disagree'] = int(row.select_one('.disagree').text)
    d['agree_ratio'] = d['agree'] / d['disagree']
    # d['agreeance_text'] = get_agreeance_text(d['agree_ratio'])

    data.append(d)
    print(d)

# website = soup.select_one('.www')['href']
# print(website)

with open('allsides.json', 'w') as f:
    json.dump(data, f)
