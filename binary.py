def binary(arr,l,r,x):

    while l<=r:
        mid=l+(r-l)/2
        if arr[mid]==x:
            return mid
        elif arr[mid]<x:
            l = mid+1
        else:
            r=mid-1
    return -1
arr=[1,3,5,6,8,9,11]
l=0
x=8
result= binary(arr,l,len(arr)-1,x)



if result != -1:
    print ("Element is present at index % d" % result)
else:
    print ("Element is not present in array")





































# def binarySearch(arr, l, r, x):
#
#     while l <= r:
#
#         mid = l + (r - l) /2; #11/2=5 #6+(11-6)/2=8
#
#         # Check if x is present at mid
#         if arr[mid] == x: #arr[8]=16
#             return mid #8
#
#         # If x is greater, ignore left half
#         elif arr[mid] < x: #arr[5]=10<16
#             l = mid + 1 #5+1=6
#
#         # If x is smaller, ignore right half
#         else: #arr[5]=10>7
#             r = mid - 1
#
#
#     # If we reach here, then the element
#     # was not present
#     return -1
#
# # Driver Code
# arr = [1,3,5,7,8,10,12,14,16,18,20,22]
# x = 16
#
# # Function call
# result = binarySearch(arr, 0, len(arr)-1, x)
#
# if result != -1:
#     print ("Element is present at index % d" % result)
# else:
#     print ("Element is not present in array")
