from twilio.rest import Client
from credentials import account_sid, auth_token,my_twilio,my_cell

# account_sid = 'AC1a6fb2e8aef34434d1617f699917281c'
# auth_token = '782fe93d1a22c2383bc100227d4f0b71'
client = Client(account_sid, auth_token)

message = client.messages.create(
                              from_=my_twilio,
                              body=''.join(['silly bob\n' for i in range(10)]),
                              to=my_cell
                          )

print(message.sid)
