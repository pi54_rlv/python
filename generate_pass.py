import random
import string

def random_char(y,type):
    if type=='weak':
        return ''.join(random.choice(string.ascii_letters) for x in range(y))
    elif type=='strong':
        return ''.join(random.choice(string.ascii_letters+string.digits+'!@#$%^&*()_' ) for x in range(y))

print (random_char(10,'strong'))
