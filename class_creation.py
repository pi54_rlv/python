class Car:

    # class attribute
    species = "road"

    # instance attribute
    def __init__(self, name, price):
        self.name = name
        self.price = price

# instantiate the Parrot class
blu = Car("Blue", 1000)
woo = Car("White", 1500)

# access the class attributes
print("Blu is a {}".format(blu.__class__.species))
print("Woo is also a {}".format(woo.__class__.species))

# access the instance attributes
print("{} is {} $".format( blu.name, blu.price))
print("{} is {} $".format( woo.name, woo.price))


class Parrot:

    # instance attributes
    def __init__(self, name, age):
        self.name = name
        self.age = age

    # instance method
    def sing(self, song):
        return "{} sings {}".format(self.name, song)

    def dance(self):
        return "{} is now dancing".format(self.name)

# instantiate the object
blu = Parrot("Blu", 10)

# call our instance methods
print(blu.sing("'Happy'"))
print(blu.dance())

print ("#################")


# parent class
class Bird:

    def __init__(self):
        print("Bird is ready")

    def whoisThis(self):
        print("Bird")

    def swim(self):
        print("Swim faster")

# child class
class Penguin(Bird):

    def __init__(self):
        # call super() function
        # super().__init__()
        print("Penguin is ready")

    def whoisThis(self):
        print("Penguin")

    def run(self):
        print("Run faster")

peggy = Penguin()
peggy.whoisThis()
peggy.swim()
peggy.run()

print ('#########')


class Person:
    "This is a person class"
    age = 10

    def greet(self):
        return ('Hello')

per=Person()
# Output: 10
print(Person.age)

# Output: <function Person.greet>
print(per.greet())

# Output: 'This is my second class'
print(Person.__doc__)
