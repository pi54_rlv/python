import requests
import pandas as pd
from bs4 import BeautifulSoup

page = requests.get('https://forecast.weather.gov/MapClick.php?lat=47.60357000000005&lon=-122.32944999999995#.X-HqL2QzZ6E')

soup = BeautifulSoup(page.content,'html.parser')

week=soup.find(id='seven-day-forecast-body')

items=week.find_all(class_='tombstone-container')


print(items[0].find(class_='period-name').get_text())
print(items[0].find(class_='short-desc').get_text())
# print(items[0].find(class_='temp').get_text())


# for item in items:
#     print(item.find(class_='period-name').get_text())


#or

period_names=[item.find(class_='period-name').get_text() for item in items]
short_desc=[item.find(class_='short-desc').get_text() for item in items]

# print(period_names)

weather_stuff=pd.DataFrame(
    {
        'period': period_names,
        'short_desc': short_desc
    })

print(weather_stuff)

weather_stuff.to_csv('weather.csv')
