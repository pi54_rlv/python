from Questio import Questio

question_prompts =[
    "What color are apples?\n(a) Red/Green\n(b) Purple\n(c) Orange\n\n",
    "What color are Bananas?\n(a) Yellow\n(b) Purple\n(c) Orange\n\n",
    "What color are oranges?\n(a) Orange\n(b) Purple\n(c) Green\n\n"
]


questions = [
    Questio(question_prompts[0],"a"),
    Questio(question_prompts[1],"a"),
    Questio(question_prompts[2],"a")
]

def run_test(questions):
    score=0
    for question in questions:
        answer=input(question.prompt)
        print(question.answer)
        if answer==question.answer:
            score+=1
    print("You got"+str(score)+"/"+str(len(questions))+"Correct")

run_test(questions)
